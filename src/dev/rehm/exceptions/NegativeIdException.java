package dev.rehm.exceptions;

public class NegativeIdException extends IllegalArgumentException {

    public NegativeIdException(){
        super();
    }

    public NegativeIdException(String message){
        super(message);
    }
}
