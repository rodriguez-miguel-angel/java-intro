package dev.rehm;

import dev.rehm.models.*;

public class DmvApp {

    public static void main(String[] args) {
        StateId id = new StateId(4890234, "John Doe", "35 main str");

        StateId id2 = new StateId(4890234, "John Doe", "35 main str");
        id2.setIdNo(-43783);
        System.out.println(id2);

        /*
        // examining equality
        System.out.println("by equals() "+ id.equals(id2));
        System.out.println("by == "+ (id==id2));

        StateId id3;
        id3 = null; // default values are not provided for method or block scoped variables
        id3.toString();

        id.equals(null);
        */


        License license = new License(7384, "Jane Doe", "25 Main St", LicenseType.GENERAL, 1);
//        System.out.println(license);

//        License license2 = new License(LicenseType.CDL, 0);
//
//
//        DmvClient client1 = new DmvClient("Driver Test", id);
//        DmvClient client2 = new DmvClient("Expired License", license);

        license.authenticate();
        int result = license.myImplementedMethod("hello");
//        System.out.println(result);

//        Authenticable a1 = new License();
//        Authenticable a2 = new Authenticable();
//        a1.getPoints();
    /*
        StateId a = new License();
        System.out.println(a.toString()); // this invokes the License toString
        System.out.println(Authenticable.myInt);

        System.out.println(a.hashCode());

     */

    }
}
