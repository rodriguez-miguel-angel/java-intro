package dev.rehm.models;

import java.io.Serializable;
import java.util.Objects;

public class DmvClient implements Serializable {

    private String reason;
    private StateId stateId;

    public DmvClient(){
        super();
    }

    public DmvClient(String reason, StateId stateId){
        super();
        this.reason = reason;
        this.stateId = stateId;
        System.out.println("invoked state id version");
    }

    public DmvClient(String reason, License stateId){
        super();
        this.reason = reason;
        this.stateId = stateId;
        System.out.println("invoked license version");
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public StateId getStateId() {
        return stateId;
    }

    public void setStateId(StateId stateId) {
        this.stateId = stateId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DmvClient dmvClient = (DmvClient) o;
        return Objects.equals(reason, dmvClient.reason) && Objects.equals(stateId, dmvClient.stateId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(reason, stateId);
    }

    @Override
    public String toString() {
        return "DmvClient{" +
                "reason='" + reason + '\'' +
                ", stateId=" + stateId +
                '}';
    }
}
