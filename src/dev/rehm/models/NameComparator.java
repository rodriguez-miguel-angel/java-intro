package dev.rehm.models;

import java.util.Comparator;

public class NameComparator implements Comparator<StateId> {

    @Override
    public int compare(StateId stateIdCard1, StateId stateIdCard2) {
        return stateIdCard1.getName().compareTo(stateIdCard2.getName());
    }
}
