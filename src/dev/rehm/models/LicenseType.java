package dev.rehm.models;

public enum LicenseType {

    CDL, GENERAL, MOTORCYCLE

}
