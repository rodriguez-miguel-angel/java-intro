package dev.rehm.models;

public interface Authenticable {

    // this variable is implicitly public static and final
    int myInt = 10;

    // I can include the abstract keyword here, but it's implicit
    public boolean authenticate();


    // While arbitrary, this method has implementation because it has the default keyword
        // and a method body
    public default int myImplementedMethod(String str){
        return 25;
    }

}
