package dev.rehm.models;

import java.util.Objects;

public class License extends StateId {

    private LicenseType type;
    private int points;

    public License(){
        super();
    }

    public License(LicenseType type, int points){
        super();
        this.type = type;
        this.points = points;
    }

    public License(int idNo, String name, String address, LicenseType type, int points){
        super(idNo, name, address);
        this.type = type;
        this.points = points;
    }

    public LicenseType getType() {
        return type;
    }

    public void setType(LicenseType type) {
        this.type = type;
    }

    public int getPoints() {
        return points;
    }

    public void setPoints(int points) {
        this.points = points;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        License license = (License) o;
        return points == license.points && type == license.type;
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), type, points);
    }

    @Override
    public String toString() {
        return "License{" +
                "idNo=" + getIdNo()+
                ", type=" + type +
                ", name=" + getName() +
                ", address=" + getAddress() +
                ", points=" + points +
                '}';
    }
}
