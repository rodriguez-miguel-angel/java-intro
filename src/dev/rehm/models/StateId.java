package dev.rehm.models;

import dev.rehm.exceptions.NegativeIdException;

import java.io.Serializable;
import java.util.Objects;

/*
    Java Bean Pattern:
    - private fields, with getters and setters
    - no arg constructor
    - implement Serializable
 */

public class StateId implements Authenticable, Serializable, Comparable<StateId> {

    // default values are provided

    private int idNo; //default 0
    private String name; // default null
    private String address; // default null

    public StateId(){
//        super();
        this(0, "n/a", "n/a");
    }

    public StateId(int idNo, String name, String address){
//        super(); // implicitly there will be a call to the super class constructor
        this.idNo = idNo;
//        setIdNo(idNo); // this method validates id is greater than 0
        this.address = address;
        this.name = name;
    }

    public int getIdNo(){
        return idNo;
    }

    public void setIdNo(int idNum) {
        if(idNum>0) {
            idNo = idNum; // this keyword is used here to prevent variable shadowing
        } else {
            throw new NegativeIdException("Cannot set ID number to "+idNum);
        }
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

//    public void extraMethod(String someText){
//        someText = someText;
//    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null | getClass() != o.getClass()) return false;
        StateId stateId = (StateId) o;
        return idNo == stateId.idNo && Objects.equals(name, stateId.name) && Objects.equals(address, stateId.address);
    }

    @Override
    public int hashCode() {
        return Objects.hash(idNo, name, address);
    }

    @Override
    public String toString(){
        return "idNo: "+idNo+", name: "+name+" , address "+ address;
    }

    @Override
    public boolean authenticate() {
        if(getIdNo()!=0){
            System.out.println("State ID was authenticated");
            return true;
        }
        return false;
    }

    @Override
    public int compareTo(StateId o) {
        return this.getIdNo() - o.getIdNo();
//        return Integer.compare(this.getIdNo(), o.getIdNo());
    }
}
