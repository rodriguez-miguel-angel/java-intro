package dev.rehm;

public enum Priority {
    HIGH, MEDIUM, LOW
}
